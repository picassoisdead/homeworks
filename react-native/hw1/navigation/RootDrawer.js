import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { RootStack } from "./RootStack";
import { CustomDrawer } from "../components";

const { Navigator, Screen } = createDrawerNavigator();

export const RootDrawer = () => {
  return (
    <NavigationContainer>
      <Navigator
        drawerStyle={{ width: 222 }}
        drawerContent={(props) => <CustomDrawer {...props} />}
      >
        <Screen
          options={{ gestureEnabled: false }}
          name="Root"
          component={RootStack}
        />
      </Navigator>
    </NavigationContainer>
  );
};
