const ADD_LIST = "ADD_LIST";
const TOGGLE_BOUGHT = "TOGGLE_BOUGHT";
const RESET_BOUGHT = "RESET_BOUGHT";
const DELETE_ITEM = "DELETE_ITEM";
const ADD_ITEM = "ADD_ITEM";
const UPDATE_ITEM = "UPDATE_ITEM";
const DELETE_LIST = "DELETE_LIST";

const MODULE_NAME = "data";
export const getLists = (state) => state[MODULE_NAME].lists;

export const getRegularLists = (state) => {
  for (let item of state[MODULE_NAME].lists) {
    if (item.listType == "regular") {
      return item.components;
    }
  }
};
export const getOneTimeLists = (state) => {
  for (let item of state[MODULE_NAME].lists) {
    if (item.listType == "onetime") {
      return item.components;
    }
  }
};

const initialState = {
  lists: [
    {
      listType: "onetime",
      components: [
        {
          id: createID(),
          name: "Everything for breakfast",
          items: [
            {
              id: createID,
              name: "Pasta",
              amount: 2,
              unit: "pkg",
              bought: false,
            },
            {
              id: createID(),
              name: "Salt",
              amount: 1,
              unit: "pkg",
              bought: false,
            },
          ],
        },
      ],
    },
    {
      listType: "regular",
      components: [
        {
          id: createID(),
          name: "Everything for dinner",
          items: [
            {
              id: createID(),
              name: "Pasta",
              amount: 2,
              unit: "pkg",
              bought: false,
            },
            {
              id: createID(),
              name: "Salt",
              amount: 1,
              unit: "pkg",
              bought: true,
            },
            {
              id: createID(),
              name: "oil",
              amount: 1,
              unit: "bott",
              bought: true,
            },
          ],
        },
      ],
    },
  ],
};

export function dataReducer(state = initialState, { type, payload }) {
  switch (type) {
    case ADD_LIST:
      return {
        ...state,
        lists: state.lists.map((list) => {
          if (list.listType === payload.listType) {
            return {
              ...list,
              components: [
                ...list.components,
                {
                  id: createID(),
                  name: payload.name,
                  items: [],
                },
              ],
            };
          }
          return list;
        }),
      };
    case DELETE_LIST:
      return {
        ...state,
        lists: state.lists.map((list) => {
          if (list.listType === payload.listType) {
            return {
              ...list,
              components: list.components.filter(
                (component) => component.id !== payload.componentID
              ),
            };
          }
          return list;
        }),
      };
    case TOGGLE_BOUGHT:
      return {
        ...state,
        lists: state.lists.map((list) => {
          if (list.listType === payload.listType) {
            return {
              ...list,
              components: list.components.map((component) => {
                if (component.id === payload.componentID) {
                  return {
                    ...component,
                    items: component.items.map((item) => {
                      if (item.id === payload.itemID) {
                        return {
                          ...item,
                          bought: item.bought ? false : true,
                        };
                      }
                      return item;
                    }),
                  };
                }
                return component;
              }),
            };
          }
          return list;
        }),
      };
    case RESET_BOUGHT:
      return {
        ...state,
        lists: state.lists.map((list) => {
          if (list.listType === payload.listType) {
            return {
              ...list,
              components: list.components.map((component) => {
                if (component.id === payload.componentID) {
                  return {
                    ...component,
                    items: component.items.map((item) => {
                      return {
                        ...item,
                        bought: false,
                      };
                    }),
                  };
                }
                return component;
              }),
            };
          }
          return list;
        }),
      };
    case DELETE_ITEM:
      return {
        ...state,
        lists: state.lists.map((list) => {
          if (list.listType === payload.listType) {
            return {
              ...list,
              components: list.components.map((component) => {
                if (component.id === payload.componentID) {
                  return {
                    ...component,
                    items: component.items.filter(
                      (item) => item.id !== payload.itemID
                    ),
                  };
                }
                return component;
              }),
            };
          }
          return list;
        }),
      };
    case ADD_ITEM:
      return {
        ...state,
        lists: state.lists.map((list) => {
          if (list.listType === payload.listType) {
            return {
              ...list,
              components: list.components.map((component) => {
                if (component.id === payload.componentID) {
                  return {
                    ...component,
                    items: [
                      ...component.items,
                      {
                        id: createID(),
                        name: payload.name,
                        amount: payload.amount,
                        unit: payload.unit,
                        bought: false,
                      },
                    ],
                  };
                }
                return component;
              }),
            };
          }
          return list;
        }),
      };
    case UPDATE_ITEM:
      return {
        ...state,
        lists: state.lists.map((list) => {
          if (list.listType === payload.listType) {
            return {
              ...list,
              components: list.components.map((component) => {
                if (component.id === payload.componentID) {
                  return {
                    ...component,
                    items: component.items.map((item) => {
                      if (item.id === payload.updateID) {
                        return {
                          ...item,
                          name: payload.name,
                          unit: payload.unit,
                          amount: payload.amount,
                        };
                      }
                      return item;
                    }),
                  };
                }
                return component;
              }),
            };
          }
          return list;
        }),
      };
    default:
      return state;
  }
}

export const addList = (payload) => ({
  type: ADD_LIST,
  payload,
});
export const deleteItem = (payload) => ({
  type: DELETE_ITEM,
  payload,
});
export const addItem = (payload) => ({
  type: ADD_ITEM,
  payload,
});

export const toggleBought = (payload) => ({
  type: TOGGLE_BOUGHT,
  payload,
});
export const resetBought = (payload) => ({
  type: RESET_BOUGHT,
  payload,
});
export const updateItem = (payload) => ({
  type: UPDATE_ITEM,
  payload,
});
export const deleteList = (payload) => ({
  type: DELETE_LIST,
  payload,
});

function createID() {
  return `${Date.now()}${Math.random()}`;
}
