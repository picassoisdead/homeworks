import React from "react";
import { connect } from "react-redux";

import { PageLayout } from "../commons/PageLayout";
import DrawerIcon from "../assets/drawer.png";
import ListCard from "../components/ListCard";
import { getLists, toggleBought, deleteList } from "../redux/data";
import { findNumberofBought } from "../utils/functions";
import { ScrollView } from "react-native-gesture-handler";

const mapStateToProps = (state) => ({
  lists: getLists(state),
});

export const RegularLists = connect(mapStateToProps, {
  toggleBought,
  deleteList,
})(({ props, lists, navigation, toggleBought, deleteList }) => {
  const regularLists = getRegularLists(lists);

  return (
    <PageLayout
      rightIcon={"ios-menu"}
      rightIconSize={32}
      navigation={navigation}
      heading="Regular Lists"
      rightIconAction={() => {
        navigation.openDrawer();
      }}
    >
      <ScrollView>
        {regularLists.components.map((component) => {
          return (
            <ListCard
              onPress={() => {
                const componentID = component.id;
                const componentName = component.name;
                const listType = "regular";
                navigation.navigate("SingleList", {
                  componentID,
                  componentName,
                  listType,
                });
              }}
              onLongPress={() =>
                deleteList({ listType: "regular", componentID: component.id })
              }
              key={component.id}
              title={component.name}
              numberOfBought={
                component.items.length == 0
                  ? 0
                  : findNumberofBought(component.items)
              }
              totalNumber={component.items.length}
            />
          );
        })}
      </ScrollView>
    </PageLayout>
  );
});
const getRegularLists = (lists) => {
  for (let item of lists) {
    if (item.listType == "regular") {
      return item;
    }
  }
};
