import React, { useState } from "react";
import { Provider } from "react-redux";
import store, { persistor } from "./redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import { AppLoading } from "expo";
import { loadFonts } from "./styles/fonts";
import { RootDrawer } from "./navigation/RootDrawer";
export default function App() {
  const [loaded, setLoaded] = useState(false);
  if (!loaded) {
    return (
      <AppLoading
        startAsync={loadFonts}
        onFinish={() => setLoaded(true)}
        onError={() => console.log("Loading failed")}
      />
    );
  }
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor} loading={null}>
        <RootDrawer />
      </PersistGate>
    </Provider>
  );
}
