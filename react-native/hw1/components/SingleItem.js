import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { CustomText } from "./CustomText";

export const SingleItem = ({ name, unit, amount, opacity, onLongPress }) => {
  return (
    <TouchableOpacity onLongPress={onLongPress}>
      <View style={[styles.container, { opacity: opacity }]}>
        <CustomText style={{ fontSize: 14 }}>{name}</CustomText>
        <CustomText style={{ fontSize: 14 }}>{`x${amount} ${unit}`}</CustomText>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    borderWidth: 2,
    borderColor: "#FFE194",
    borderRadius: 27,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 21,
    height: 40,
    marginTop: 15,
  },
});
