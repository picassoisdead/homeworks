import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  AsyncStorage,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import userImg from "../assets/user.png";
import { CustomText } from "./CustomText";
const getUser = async (setValue) => {
  try {
    const username = await AsyncStorage.getItem("value");
    if (username !== null) {
      const a = JSON.parse(username);
      setValue(a);
    }
  } catch (error) {}
};

export const CustomDrawer = ({ navigation }) => {
  const [user, setUser] = useState("Username");
  const [image, setImage] = useState("");

  const getUser = async () => {
    try {
      const userInfo = await AsyncStorage.getItem("userInfo");
      if (userInfo !== null) {
        const a = JSON.parse(userInfo);
        setUser(a.value);
        setImage(a.url);
      }
    } catch (error) {}
  };
  getUser();

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.userInfo}>
        <Image
          style={styles.userImg}
          source={image ? { uri: image } : userImg}
        />
        <CustomText style={styles.username}>{user}</CustomText>
      </View>
      <View style={styles.screens}>
        <TouchableOpacity
          style={styles.newList}
          onPress={() => navigation.navigate("Create")}
        >
          <CustomText weight="bold" style={styles.newListText}>
            Add new list
          </CustomText>
        </TouchableOpacity>
        <View style={{ marginTop: 32 }}>
          <TouchableOpacity
            style={styles.newList}
            onPress={() => navigation.navigate("OneTime")}
          >
            <CustomText weight="bold" style={styles.newListText}>
              One time list
            </CustomText>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.newList}
            onPress={() => navigation.navigate("Regular")}
          >
            <CustomText weight="bold" style={styles.newListText}>
              Regular lists
            </CustomText>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.newList}
            onPress={() => navigation.navigate("Settings")}
          >
            <CustomText weight="bold" style={styles.newListText}>
              User Settings{" "}
            </CustomText>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userInfo: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 15,
    marginBottom: 10,
  },
  userImg: {
    borderRadius: 100,
    width: 50,
    height: 50,
  },
  username: {
    marginLeft: 20,
    fontSize: 22,
    color: "#303234",
  },
  screens: {
    backgroundColor: "#FF7676",
    borderTopEndRadius: 15,
    borderTopStartRadius: 15,
    height: "100%",
    paddingHorizontal: 15,
  },
  newList: {
    marginTop: 16,
    backgroundColor: "white",
    borderRadius: 20,
  },
  newListText: {
    color: "#FF7676",
    marginVertical: 7,
    textTransform: "uppercase",
    textAlign: "center",
  },
});
