import React from "react";
import {
  Text,
  View,
  StyleSheet,
  Button,
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform,
} from "react-native";
import { CustomText } from "./CustomText";

export const PrimaryButton = ({
  title,
  onPress,
  style,
  cheight,
  cFontSize,
}) => {
  const Touchable =
    Platform.OS === "android" ? TouchableNativeFeedback : TouchableOpacity;
  return (
    <View style={[styles.container, style]}>
      <Touchable onPress={onPress}>
        <View style={[styles.btn, { height: cheight }]}>
          <CustomText
            weight="bold"
            style={[styles.title, { fontSize: cFontSize }]}
          >
            {title}
          </CustomText>
        </View>
      </Touchable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  btn: {
    backgroundColor: "#ff7676",
    borderRadius: 39,
    justifyContent: "center",
  },
  title: {
    color: "white",
    textTransform: "uppercase",
    textAlign: "center",
  },
});
