import React from "react";
import { StyleSheet, Text, View } from "react-native";
import * as Progress from "react-native-progress";
import { CustomText } from "./CustomText";
import { TouchableOpacity } from "react-native-gesture-handler";

const ListCard = ({
  title,
  numberOfBought,
  totalNumber,
  onPress,
  onLongPress,
}) => {
  return (
    <TouchableOpacity
      onLongPress={onLongPress}
      onPress={onPress}
      style={styles.container}
    >
      <View style={{ opacity: numberOfBought === totalNumber ? 0.3 : 1 }}>
        <View style={styles.title}>
          <CustomText
            weight="semi"
            style={{
              fontSize: 18,
            }}
          >
            {title}
          </CustomText>
          <CustomText
            weight="semi"
            style={{
              fontSize: 14,
            }}
          >{`${numberOfBought} / ${totalNumber}`}</CustomText>
        </View>
        <Progress.Bar
          color={"#FFD976"}
          unfilledColor={"#eee"}
          style={styles.progbar}
          progress={totalNumber === 0 ? 0 : numberOfBought / totalNumber}
          width={null}
          height={19}
          borderRadius={20}
          borderWidth={0}
        />
      </View>
    </TouchableOpacity>
  );
};

export default ListCard;

const styles = StyleSheet.create({
  container: {
    borderWidth: 2,
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderRadius: 10,
    borderColor: "#ffe194",
    marginBottom: 13,
  },

  title: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 5,
    paddingHorizontal: 2,
  },
});
