import React, { useState } from 'react';
import { Button } from './components/Button';
import { Modal } from './components/Modal';


function App() {

  const [firstModalStatus, setFirstModalStatus] = useState(false);
  const [secondModalStatus, setSecondModalStatus] = useState(false);
  const toggleFirstModal = () => setFirstModalStatus(v=> !v);
  const toggleSecondModal = () => setSecondModalStatus(v=> !v);
  return (
    <div className="App" style = {{height: 400, display: 'flex', alignItems: 'center',  flexDirection: 'column'}}>
      <div>
      <Button backgroundColor = 'rgba(255,0,0, .8)' text = "Open first modal window" onClick = {() => {setFirstModalStatus(true);setSecondModalStatus(false)}}/>
      <Button backgroundColor = 'darkgreen' text = "Open second modal window" onClick = {() => {setSecondModalStatus(true);setFirstModalStatus(false)}}/>
      </div>
      { firstModalStatus && (
        <Modal 
          header = {'Test'}
          closeButton = {true}
          text = {'Hello World'}
          close = {toggleFirstModal}
          actions = {[<Button 
                          key = {1}
                          backgroundColor = 'rgba(0,0,0, .2)'
                          text = 'Ok'
                          onClick = {() => alert('first modal window')}/>,
                      <Button 
                          key = {2}
                          backgroundColor = 'rgba(0,0,0, .2)'
                          text = 'Cancel'
                          onClick = {toggleFirstModal}/>]}/>
      )}
      { secondModalStatus && (
        <Modal 
          header = {'Test'}
          closeButton = {true}
          text = {'Once you delete this file, it will not be possible to undo this action. \n\n Are you sure you want to delete it?'}
          close = {toggleSecondModal}
          actions = {[<Button 
                          key = {1}
                          backgroundColor = 'rgba(0,0,0, .2)'
                          text = 'Ok'
                          onClick = {() => alert('second modal window')}/>,
                      <Button 
                          key = {2}
                          backgroundColor = 'rgba(0,0,0, .2)'
                          text = 'Cancel'
                          onClick = {toggleSecondModal}/>]}/>
      )}
    </div>
    
  );
}
export default App;