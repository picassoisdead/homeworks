import React, {useReducer} from 'react';

const initialState = {
	status: false,
  	list: [1],
  	users: [
      {id:1, name: 'John', age: 18},
      {id:2, name: 'Sara', age: 20},
      {id:3, name: 'Mary', age: 24}
    ]
};

function reducer(state, {type, payload}) {
		switch (type) {
			case 'toggle_status':
				return {
					...state,
					status: state.status ? false : true 
				}
			case 'add_number_to_list':
				return {
					...state,
					list: [...state.list, payload]
				}
			case 'clear_list':
				return {
					...state,
					list: []
				}
			case 'change_age':
				return {
					...state,
					users: payload
				}
			default: 
				return state
		}
}

export const TestReducerComponent = () => {
  	const [state, dispatch] = useReducer(reducer,initialState);
  	const {status, list, users} = state;
	console.log(users);
	
	
  	const toggleStatus = () => {
    	dispatch({
			type: 'toggle_status'
		})
    }
        
    const addNumberToList = (number) => {
    	dispatch({
			type: 'add_number_to_list',
			payload: number
		})
    }
	
    const clearList = () => {
    	dispatch({
			type: 'clear_list'
			
		})
    }
        
    const changeUserAge = (id, age) => {
        const newUsers=[...state.users];
        newUsers[id-1].age = age;
        dispatch(({type: 'change_age', payload: newUsers}));   
     }

	return (
		<div>
        	<h1>Status is {status ? 'true' : 'false'}</h1>
			<button onClick={toggleStatus}>toggle status</button>
			<button onClick={()=>{addNumberToList(Date.now())}}>Add random number to the list</button>
			<button onClick={()=>{clearList()}}>Clear List</button>
			<button onClick={()=>{changeUserAge(1,55)}}>Change user 1 age to 55</button>
			
        	<ul>
              {list.map(i => (<li key={i}>{i}</li>))}
			</ul>
        	<ol>
              {users.map(({id,name,age}) => <li key={id}>{name} / {age}</li>)}
			</ol>
      	</div>
	)
}