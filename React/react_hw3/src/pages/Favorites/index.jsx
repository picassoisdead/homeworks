import React from 'react';

import {ProductsList} from '../../components/ProductsList'

export const Favorites = () => {

    const data = [];

    for (const key in localStorage) {
        if (key.includes('fav_item')) {
            data.push(JSON.parse(localStorage[key]));
        }
    }
    
    
    
    


    return (
        <ProductsList fav={true} data={data}/>
    )

}