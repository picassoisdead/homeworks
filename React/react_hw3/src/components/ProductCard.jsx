import React, { useState, useContext } from "react";
import { Modal, Button } from "../components";

export const ProductCard = ({
  name,
  price,
  image,
  description,
  id,
  cart,
  fav,
}) => {
  const checkLocalStorage = (id) => {
    const fav_items = [];
    for (const key in localStorage) {
      if (key.includes("fav_item")) {
        fav_items.push(JSON.parse(localStorage[key]));
      }
    }
    return fav_items.find((item) => (item.id === id ? true : false));
  };

  const [favss, setFavss] = useState(checkLocalStorage(id));
  const [remove, setRemove] = useState(false);
  const [deleted, setDeleted] = useState(false);
  const [firstModalStatus, setFirstModalStatus] = useState(false);

  const toggleFirstModal = () => setFirstModalStatus((v) => !v);

  return (
    <>
      {!deleted && !remove && (
        <>
          <div className="product-card">
            <img className="album-cover" src={image} alt="" />
            <div className="card-middle">
              <h3>{name}</h3>
              <span>{description}</span>
            </div>
            <div className="card-bottom">
              <h3>${price}</h3>
              <div className="add-to-fav-icon">
                <svg
                  fill={favss ? "orange" : "black"}
                  onClick={() => {
                    if (!favss) {
                      localStorage.setItem(
                        `fav_item${id}`,
                        JSON.stringify({ name, price, image, description, id })
                      );
                      setFavss(checkLocalStorage(id));
                    } else {
                      localStorage.removeItem(`fav_item${id}`);
                      setFavss(checkLocalStorage(id));
                      if (fav) {
                        setRemove(true);
                      }
                    }
                  }}
                  height="20px"
                  viewBox="0 -10 511.99143 511"
                  width="20px"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="m510.652344 185.882812c-3.371094-10.367187-12.566406-17.707031-23.402344-18.6875l-147.796875-13.417968-58.410156-136.75c-4.3125-10.046875-14.125-16.53125-25.046875-16.53125s-20.738282 6.484375-25.023438 16.53125l-58.410156 136.75-147.820312 13.417968c-10.835938 1-20.011719 8.339844-23.402344 18.6875-3.371094 10.367188-.257813 21.738282 7.9375 28.925782l111.722656 97.964844-32.941406 145.085937c-2.410156 10.667969 1.730468 21.699219 10.582031 28.097656 4.757813 3.457031 10.347656 5.183594 15.957031 5.183594 4.820313 0 9.644532-1.28125 13.953125-3.859375l127.445313-76.203125 127.421875 76.203125c9.347656 5.585938 21.101562 5.074219 29.933593-1.324219 8.851563-6.398437 12.992188-17.429687 10.582032-28.097656l-32.941406-145.085937 111.722656-97.964844c8.191406-7.1875 11.308594-18.535156 7.9375-28.925782zm-252.203125 223.722657" />
                </svg>
              </div>
              {!cart ? (
                <button onClick={toggleFirstModal} className="add-to-cart">
                  Add to cart
                </button>
              ) : (
                <button onClick={toggleFirstModal} className="remove-from-cart">
                  Remove from cart
                </button>
              )}
            </div>
          </div>
          {firstModalStatus && (
            <div className="modal-background">
              {!cart ? (
                <Modal
                  header={"Confirmation"}
                  closeButton={true}
                  text={"Do you want to add this item to your cart?"}
                  close={toggleFirstModal}
                  actions={[
                    <ModalButton
                      key={1}
                      backgroundColor="rgba(0,0,0, .2)"
                      text="Confirm"
                      onClick={() => {
                        toggleFirstModal();
                        localStorage.setItem(
                          `cart_item${id}`,
                          JSON.stringify({
                            name,
                            price,
                            image,
                            description,
                            id,
                          })
                        );
                      }}
                    />,
                    <ModalButton
                      key={2}
                      backgroundColor="rgba(0,0,0, .2)"
                      text="Cancel"
                      onClick={toggleFirstModal}
                    />,
                  ]}
                />
              ) : (
                <Modal
                  header={"Confirmation"}
                  closeButton={true}
                  text={"Do you want to remove this item to from cart?"}
                  close={toggleFirstModal}
                  actions={[
                    <Button
                      key={1}
                      backgroundColor="rgba(0,0,0, .2)"
                      text="Confirm"
                      onClick={() => {
                        toggleFirstModal();
                        localStorage.removeItem(`cart_item${id}`);
                        setDeleted(true);
                      }}
                    />,
                    <Button
                      key={2}
                      backgroundColor="rgba(0,0,0, .2)"
                      text="Cancel"
                      onClick={toggleFirstModal}
                    />,
                  ]}
                />
              )}
            </div>
          )}
        </>
      )}
    </>
  );
};
