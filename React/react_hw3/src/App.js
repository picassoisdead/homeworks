import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import {Homepage, Favorites, Cart, TestReducerComponent} from './pages/index'
import {Header} from "./commons/Header"

function App() {

  


  return (
    <div>
        <Router>
            <Header/>
              <Switch>
                <Route exact path="/" component={Homepage}/>
                <Route path="/cart" component={Cart}/>
                <Route path="/favs" component={Favorites}/>
                <Route path="/task" component={TestReducerComponent}/>
              </Switch>
        </Router>
    </div>
  );
}

export default App;
